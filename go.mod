module gitlab.com/sphipu/faire

go 1.13

require (
	github.com/dlclark/regexp2 v1.2.0 // indirect
	github.com/dop251/goja v0.0.0-20200126140343-a7d7d4827263
	github.com/fsnotify/fsnotify v1.4.7
	github.com/go-sourcemap/sourcemap v2.1.2+incompatible // indirect
	golang.org/x/sys v0.0.0-20200202164722-d101bd2416d5 // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
