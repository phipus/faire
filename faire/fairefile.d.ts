type FaireCallback = (target: string, matches: string[]) => void
declare interface DirAPI {
    create(dirName: string): void
    exists(dirName: string): boolean
    remove(dirName: string): void
    list(dirName: string, recurse?: boolean): string[]
    listMatch(dirName: string, recurse?: boolean): string[]
}

declare interface FileAPI {
    read(fileName: string): string
    write(fileName: string, content: string): void
    exists(fileName: string): void
    remove(fileName: string): void
}

declare interface PathAPI {
    base(path: string): string
    clean(path: string): string
    dir(path: string): string
    ext(path: string): string
    isAbs(path: string): boolean
    join(...paths: string[]): string
    match(pattern: string, path: string): boolean
    split(path: string): [string, string]
}

declare interface EnvAPI {
    get(name: string): string
    set(name: string, value: string): string
    isSet(name: string): boolean
    expand(s: string): string
}

declare interface ExecOptions {
    pipeStdout: boolean
    pipeStderr: boolean
    stdin: string
}

declare interface ExecResult {
    stdout: string
    stderr: string
}

declare function faire(rule: string|RegExp, cb: FaireCallback): void
declare function requise(target: string): void
declare function log(...v: string[]): void
declare function obsolete(): boolean
declare function exec(program: string, args: string[], config?:ExecOptions): ExecResult
declare function phony(...target: string[]): void

declare const dir: DirAPI
declare const file: FileAPI
declare const path: PathAPI
declare const env: EnvAPI
declare const operatingSystem: string
declare let extendedErrors: boolean
declare let workDir: string