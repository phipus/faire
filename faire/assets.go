// Package main Code generated by go-bindata. (@generated) DO NOT EDIT.
// sources:
// cli/fairefile.d.ts
// cli/fairefile-template.txt
package main

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func bindataRead(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	clErr := gz.Close()

	if err != nil {
		return nil, fmt.Errorf("read %q: %v", name, err)
	}
	if clErr != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes []byte
	info  os.FileInfo
}

type bindataFileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
}

// Name return file name
func (fi bindataFileInfo) Name() string {
	return fi.name
}

// Size return file size
func (fi bindataFileInfo) Size() int64 {
	return fi.size
}

// Mode return file mode
func (fi bindataFileInfo) Mode() os.FileMode {
	return fi.mode
}

// ModTime return file modify time
func (fi bindataFileInfo) ModTime() time.Time {
	return fi.modTime
}

// IsDir return file whether a directory
func (fi bindataFileInfo) IsDir() bool {
	return fi.mode&os.ModeDir != 0
}

// Sys return file is sys mode
func (fi bindataFileInfo) Sys() interface{} {
	return nil
}

var _fairefileDTs = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x94\x54\xcd\x6e\xdb\x3c\x10\xbc\x1b\xf0\x3b\xf0\x28\x03\x86\x1f\x40\x40\xbe\x20\xf8\xe2\x00\x3d\xb4\x0d\xe2\x63\xd0\x03\x45\xad\xe5\x6d\x68\x92\x5d\xae\x14\x19\x6d\xdf\xbd\x60\x2c\x59\x12\xf5\x83\xf6\x64\x43\x3b\x3b\xdc\x1d\x0e\x87\x2f\x0e\xc4\x93\x44\x82\xff\xa5\xd6\x99\x54\x6f\xe2\x4e\x24\x2c\xa9\x00\x4e\x85\x67\x42\x53\x6c\xc5\x59\xb2\x3a\x81\x6f\x3f\xbc\x7e\xdb\x88\xbb\xff\x44\x65\x31\x5f\xaf\x72\x50\x5a\x12\x08\x34\x0c\x74\x94\x0a\xc4\x23\xd2\xc3\xf3\x27\xf1\x73\xbd\x12\x42\x08\x45\x20\x19\x92\x1c\xe9\x8b\x3c\x43\x4b\xb1\x49\x9b\xf6\x00\x81\x1a\x3d\xfb\x09\x48\x66\xad\x06\x69\xae\x28\x82\xb3\xad\x96\x89\x34\x7a\x8e\x01\x5b\x41\xa0\x4a\xf2\x70\x7f\xe3\xdb\x74\x8b\x74\x7d\x9f\xc3\x8e\xff\xda\xfc\x7b\xbd\x9a\x52\xe0\x09\x35\xf4\x24\x20\x90\x79\x72\x44\x0d\xd1\xe0\xd7\x3f\x57\xcc\x3b\x21\xc3\x08\xb4\x15\xca\x1a\x06\xc3\x4b\xba\x4d\x30\x77\x98\x46\xb5\x59\xcc\xcc\x06\xcf\x92\x4f\xbd\x0d\x32\xe9\x21\x71\x92\x4f\x33\xd3\xab\xa0\xcc\x12\x20\x47\x5a\x2a\x43\xcd\x4b\x65\xf4\x0f\x99\x8f\x01\x03\x73\x7c\xb7\x68\x92\xdd\x6e\x17\x30\x7d\x9f\x0e\x79\x3e\x7c\x1c\x78\x18\xc8\x74\x1a\x2f\x11\x7b\xa7\x71\x34\xdb\x6b\xdb\x79\xfd\x9d\x37\xc2\xde\x54\x3d\x15\x0b\xe0\xc4\xcc\x5b\xc0\x47\xe5\xad\xa8\xa4\x2e\xe7\xd0\xe8\x0f\x63\xba\xc1\xe8\x50\x3b\x69\xf2\xc4\x4f\x10\xcc\x8d\x5b\x83\xfa\xea\x18\xad\xf1\xed\xcc\x0e\x1d\x1c\x38\xb7\x25\x47\xec\x4d\x01\x88\x62\xc5\x38\x47\xf3\x57\x47\xbd\x80\x2f\x35\xb7\x27\xf9\xe6\x94\x81\x24\xcd\x01\x93\x64\xc7\xd2\xa8\x30\xab\x38\x86\xfc\x4a\xa8\xd4\x37\x29\x7e\xbd\x40\xb1\xaf\xdd\x56\xa8\x2c\x1d\xc6\xdb\xcd\xf8\x23\x16\x82\x1f\x25\x7a\x88\xc2\x6f\x1e\xaf\x6d\x11\x1c\x57\x0d\xdc\x36\x83\xb5\x99\xb7\x1a\x18\x92\xfe\x1d\x8d\x50\x50\x83\x4a\x1c\xd9\x82\xe4\xb9\x33\x81\xa4\xa2\xe7\xe8\x8f\x44\x38\x62\x71\x9f\xf6\x2e\x6b\x93\xf6\xf4\x9c\x20\x76\x27\x6b\x2e\x61\xd8\xe1\x6e\xbd\x89\xbb\x26\x65\x8d\xe7\xf0\x5e\xd3\x26\xc8\xe3\x52\x88\x92\xb4\x8d\xb8\xb8\x78\x7d\x28\x4d\x7a\xc4\x45\x30\x55\xda\x3c\x89\xb8\x64\x1d\x90\x64\x34\xc5\xe1\xe2\x19\xce\xdd\x8d\xb7\x30\x0d\x1c\x52\x02\x4c\x0e\xf9\x9e\xc8\x92\x9f\x50\x32\x80\xde\x2d\xbd\x3d\xe2\xcd\x33\x7f\x02\x00\x00\xff\xff\x62\xe3\xf8\x4c\xe1\x06\x00\x00")

func fairefileDTsBytes() ([]byte, error) {
	return bindataRead(
		_fairefileDTs,
		"fairefile.d.ts",
	)
}

func fairefileDTs() (*asset, error) {
	bytes, err := fairefileDTsBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "fairefile.d.ts", size: 1761, mode: os.FileMode(438), modTime: time.Unix(1580918086, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _fairefileTemplateTxt = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xd2\xd7\xd7\x57\xb0\x29\x4a\x4d\x4b\x2d\x4a\xcd\x4b\x4e\x55\x28\x48\x2c\xc9\xb0\x55\xd2\x4b\x4b\xcc\x2c\x4a\xd5\x07\x93\x69\x99\x39\xa9\x7a\x29\x7a\x25\xc5\x4a\x0a\xfa\x76\xbc\x5c\x80\x00\x00\x00\xff\xff\x7e\xca\x65\xca\x30\x00\x00\x00")

func fairefileTemplateTxtBytes() ([]byte, error) {
	return bindataRead(
		_fairefileTemplateTxt,
		"fairefile-template.txt",
	)
}

func fairefileTemplateTxt() (*asset, error) {
	bytes, err := fairefileTemplateTxtBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "fairefile-template.txt", size: 48, mode: os.FileMode(438), modTime: time.Unix(1580906341, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// MustAsset is like Asset but panics when Asset would return an error.
// It simplifies safe initialization of global variables.
func MustAsset(name string) []byte {
	a, err := Asset(name)
	if err != nil {
		panic("asset: Asset(" + name + "): " + err.Error())
	}

	return a
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"fairefile.d.ts":         fairefileDTs,
	"fairefile-template.txt": fairefileTemplateTxt,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for childName := range node.Children {
		rv = append(rv, childName)
	}
	return rv, nil
}

type bintree struct {
	Func     func() (*asset, error)
	Children map[string]*bintree
}

var _bintree = &bintree{nil, map[string]*bintree{
	"fairefile-template.txt": &bintree{fairefileTemplateTxt, map[string]*bintree{}},
	"fairefile.d.ts":         &bintree{fairefileDTs, map[string]*bintree{}},
}}

// RestoreAsset restores an asset under the given directory
func RestoreAsset(dir, name string) error {
	data, err := Asset(name)
	if err != nil {
		return err
	}
	info, err := AssetInfo(name)
	if err != nil {
		return err
	}
	err = os.MkdirAll(_filePath(dir, filepath.Dir(name)), os.FileMode(0755))
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
	if err != nil {
		return err
	}
	err = os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
	if err != nil {
		return err
	}
	return nil
}

// RestoreAssets restores an asset under the given directory recursively
func RestoreAssets(dir, name string) error {
	children, err := AssetDir(name)
	// File
	if err != nil {
		return RestoreAsset(dir, name)
	}
	// Dir
	for _, child := range children {
		err = RestoreAssets(dir, filepath.Join(name, child))
		if err != nil {
			return err
		}
	}
	return nil
}

func _filePath(dir, name string) string {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	return filepath.Join(append([]string{dir}, strings.Split(cannonicalName, "/")...)...)
}
