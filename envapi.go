package faire

import (
	"os"

	"github.com/dop251/goja"
)

func (f *F) newEnvAPI() goja.Value {
	api := f.rt.NewObject()
	api.Set("get", f.jsEnvGet)
	api.Set("set", f.jsEnvSet)
	api.Set("isSet", f.jsEnvIsSet)
	api.Set("expand", f.jsEnvExpand)
	return api
}

func (f *F) jsEnvGet(name string) string {
	return os.Getenv(name)
}

func (f *F) jsEnvSet(name, value string) {
	err := os.Setenv(name, value)
	if err != nil {
		f.throwErr("Can not set environment variable "+name, err)
	}
}

func (f *F) jsEnvIsSet(name string) bool {
	_, ok := os.LookupEnv(name)
	return ok
}

func (f *F) jsEnvExpand(s string) string {
	return os.ExpandEnv(s)
}
