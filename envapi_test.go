package faire

import (
	"os"
	"testing"
)

func TestEnvGet(t *testing.T) {
	faire := New()
	path := os.Getenv("PATH")

	result, err := faire.rt.RunString(`env.get("PATH")`)
	if err != nil {
		t.Error(err)
		return
	}
	if result.Export().(string) != path {
		t.Error("env.get(1) did return bad data")
	}
}

func TestEnvSet(t *testing.T) {
	faire := New()
	defer os.Unsetenv("_TEST_ENVVAR")
	_, err := faire.rt.RunString(`env.set("_TEST_ENVVAR", "__value__")`)
	if err != nil {
		t.Error(err)
		return
	}

	if os.Getenv("_TEST_ENVVAR") != "__value__" {
		t.Error("env.set(2) did not work as expected")
	}
}

func TestEnvIsSet(t *testing.T) {
	faire := New()
	defer os.Unsetenv("_TEST_IS_SET")
	os.Setenv("_TEST_IS_SET", "OK")

	result, err := faire.rt.RunString(`env.isSet("_TEST_IS_SET")`)
	if err != nil {
		t.Error(err)
		return
	}
	if !result.ToBoolean() {
		t.Error("env.isSet(1) reported that _TEST_IS_SET is not set")
		return
	}

	result, err = faire.rt.RunString(`env.isSet("_TEST_IS_NOT_SET")`)
	if err != nil {
		t.Error(err)
		return
	}

	if result.ToBoolean() {
		t.Error("env.isSet(1) reported that _TEST_IS_NOT_SET is set")
		return
	}
}

func TestEnvExpand(t *testing.T) {
	value := os.Getenv("PATH")
	faire := New()

	result, err := faire.rt.RunString(`env.expand("$PATH")`)
	if err != nil {
		t.Error(err)
		return
	}

	if result.Export().(string) != value {
		t.Error("env.expand(1) did not expand PAHT correctly")
		return
	}
}
