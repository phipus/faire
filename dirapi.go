package faire

import (
	"io/ioutil"
	"os"
	"path"

	"github.com/dop251/goja"
)

func (f *F) newDirAPI() goja.Value {
	api := f.rt.NewObject()
	api.Set("create", f.jsDirCreate)
	api.Set("exists", f.jsDirExists)
	api.Set("remove", f.jsDirRemove)
	api.Set("list", f.jsDirList)
	api.Set("listMatch", f.jsDirListMatch)

	return api
}

func (f *F) jsDirCreate(dirName string) {
	err := os.MkdirAll(f.absPath(dirName), os.ModePerm)
	if err != nil {
		f.throwErr("Can not create directory "+dirName, err)
	}
}

func (f *F) jsDirExists(dirName string) bool {
	stat, err := os.Stat(f.absPath(dirName))
	if err != nil {
		return false
	}
	return stat.IsDir()
}

func (f *F) jsDirRemove(dirName string) {
	err := os.RemoveAll(f.absPath(dirName))
	if err != nil {
		f.throwErr("Can not remove directory "+dirName, err)
	}
}

func (f *F) jsDirList(dirName string, recurse bool) []string {
	result := []string{}
	children, err := ioutil.ReadDir(f.absPath(dirName))
	if err != nil {
		f.throwErr("Can not list directory "+dirName, err)
	}

	for _, child := range children {
		childName := path.Join(dirName, child.Name())
		result = append(result, childName)
		if recurse && child.IsDir() {
			result = append(result, f.jsDirList(childName, true)...)
		}
	}

	return result
}

func (f *F) jsDirListMatch(dirName, pattern string, recurse bool) []string {
	result := []string{}
	children, err := ioutil.ReadDir(f.absPath(dirName))
	if err != nil {
		f.throwErr("Can not list directory "+dirName, err)
	}

	for _, child := range children {
		childName := path.Join(dirName, child.Name())
		isMatch, err := path.Match(pattern, childName)
		if err != nil {
			f.throwErr("Invalid match pattern "+pattern, err)
		}
		if isMatch {
			result = append(result, childName)
		}
		if recurse && child.IsDir() {
			result = append(result, f.jsDirListMatch(childName, pattern, true)...)
		}
	}

	return result
}
