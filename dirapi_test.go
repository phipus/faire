package faire

import (
	"io/ioutil"
	"os"
	"testing"
)

const (
	testDir = "_testdir"
)

var exampleDirs = []string{"_testdir/example1", "_testdir/example2", "_testdir/example3"}
var exampleFiles = []string{"_testdir/example1/test.txt", "_testdir/example2/test2.txt", "_testdir/testfile.txt"}

func createExampleDirsAndFiles() error {
	for _, name := range exampleDirs {
		err := os.MkdirAll(name, os.ModePerm)
		if err != nil {
			return err
		}
	}

	for _, name := range exampleFiles {
		err := ioutil.WriteFile(name, []byte(name), 0644)
		if err != nil {
			return err
		}
	}
	return nil
}

func removeExampleDirsAndFiles() {
	for _, name := range exampleFiles {
		os.Remove(name)
	}
	for _, name := range exampleDirs {
		os.RemoveAll(name)
	}
}

func isDir(name string) bool {
	stat, err := os.Stat(name)
	return err == nil && stat.IsDir()
}

func removeDirs(names []string) {
	for _, name := range names {
		os.RemoveAll(name)
	}
}

func isFile(name string) bool {
	stat, err := os.Stat(name)
	return err == nil && stat.Mode().IsRegular()
}

// TestDirCreate tests if dir.create creates a new directory
func TestDirCreate(t *testing.T) {
	defer os.Remove("_testdir/test_create")
	faire := New()
	_, err := faire.rt.RunString(`dir.create("_testdir/test_create")`)
	if err != nil {
		t.Error(err)
		return
	}

	if !isDir("_testdir/test_create") {
		t.Error("_testdir/test_create was not created")
		return
	}

	// dir.create must not fail if the directory already exists
	_, err = faire.rt.RunString(`dir.create("_testdir/test_create")`)
	if err != nil {
		t.Error(err)
		return
	}

}

func TestDirRemove(t *testing.T) {
	faire := New()
	err := os.MkdirAll("_testdir/test_remove", os.ModePerm)
	if err != nil {
		t.Error(err)
		return
	}

	_, err = faire.rt.RunString(`dir.remove("_testdir/test_remove")`)
	if err != nil {
		t.Error(err)
		return
	}

	if isDir("_testdir/test_remove") {
		t.Error("_testdir/test_remove was not deleted")
		return
	}
}

func TestDirExists(t *testing.T) {
	defer os.Remove("_testdir/test_exists")
	faire := New()
	err := os.MkdirAll("_testdir/test_exists", os.ModePerm)
	if err != nil {
		t.Error(err)
		return
	}

	result, err := faire.rt.RunString(`dir.exists("_testdir/test_exists")`)
	if err != nil {
		t.Error(err)
		return
	}
	if !result.ToBoolean() {
		t.Error("dir.exists(1) reported that _testdir/test_exists does not exist (but it does)")
		return
	}
}

func TestDirList(t *testing.T) {
	defer removeExampleDirsAndFiles()
	err := createExampleDirsAndFiles()
	if err != nil {
		t.Error(err)
		return
	}

	faire := New()
	result, err := faire.rt.RunString(`dir.list("_testdir")`)
	if err != nil {
		t.Error(err)
		return
	}

	if result.ToObject(faire.rt).Get("length").ToInteger() != int64(len(exampleDirs)+1) {
		t.Log(result.String())
		t.Error("dir.list(1) did not return all children")
		return
	}
}

func TestDirListRecurse(t *testing.T) {
	defer removeExampleDirsAndFiles()
	err := createExampleDirsAndFiles()
	if err != nil {
		t.Error(err)
		return
	}

	faire := New()
	result, err := faire.rt.RunString(`dir.list("_testdir", true)`)
	if err != nil {
		t.Error(err)
		return
	}

	if result.ToObject(faire.rt).Get("length").ToInteger() != int64(len(exampleDirs)+len(exampleFiles)) {
		t.Error("dir.list(2) did not return all children")
		return
	}
}

func TestDirListMatch(t *testing.T) {
	defer removeExampleDirsAndFiles()
	err := createExampleDirsAndFiles()
	if err != nil {
		t.Error(err)
		return
	}

	faire := New()
	result, err := faire.rt.RunString(`dir.listMatch("_testdir", "_testdir/*/*.txt", true)`)
	if err != nil {
		t.Error(err)
		return
	}

	if result.ToObject(faire.rt).Get("length").ToInteger() != 2 {
		t.Error("dir.listMatch(3) did not return all children ")
		return
	}
}
